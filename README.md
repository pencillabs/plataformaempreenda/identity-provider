# Identity Provider

O gerenciamento de autenticação permite que usuários se registrem e se autentiquem em diversas plataformas usando apenas um login, por meio do Single Sign On (SSO). Na Plataforma Empreenda será usado
o provedor de identidades de código aberto [Keycloak](https://www.keycloak.org/). Assim, será possível com apenas um login, entrar no Moodle e no Blog do projeto.

Este repositório é responsável por criar e configurar o Keycloak.  

## Configurando o projeto com o Docker

**1. Clone o repositório**
```sh
     git clone https://gitlab.com/pencillabs/plataformaempreenda/identity-provider.git
     cd identity-provider
```

**2. Instalar o [docker compose plugin](https://docs.docker.com/compose/install/linux/#install-using-the-repository)**
**3. Crie um arquivo .env a partir do arquivo .sample.env**

    cp .sample.env .env

O usuário e a senha de administração do Keycloak, além de outras variáveis que configuram a aplicação, são definidas nesse arquivo.

**Exemplo de configuração**

```
KC_BOOTSTRAP_ADMIN_USERNAME=admin
KC_BOOTSTRAP_ADMIN_PASSWORD=admin
KC_HOSTNAME_PORT=8080
KC_HOSTNAME=http://192.168.15.133:${KC_HOSTNAME_PORT}
VIRTUAL_HOST=http://192.168.15.133:${KC_HOSTNAME_PORT}
LETSENCRYPT_HOST=http://192.168.15.133:${KC_HOSTNAME_PORT}
KC_HOSTNAME_STRICT_BACKCHANNEL=false
KC_HTTP_ENABLED=true
KC_HOSTNAME_STRICT_HTTPS=false
KC_HEALTH_ENABLED=true
PROXY_ADDRESS_FORWARDING=true
```

**4. Crie os containers da aplicação**
```sh
docker-compose up
```
Acesse a aplicação no endereço http://localhost:8080.

Para mais informações, acesse a documentação completa no repositório do site.
