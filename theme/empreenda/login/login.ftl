<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=false displayMessage=false; section>
    <#if section == "title">
        ${msg("loginTitle", (realm.displayName!''))}
    <#elseif section == "form">
        <#if realm.password>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <div class="min-vh-100 min-vw-100">
                <div class="background-image position-relative">
                    <div class="d-flex justify-content-center align-items-center w-100">
                    
                        <div class="card mt-4 p-4 bg-transparent border-0">
                            <div class="card-body text-center">
                                <!-- Title (Login) -->
                                <h1 class="mb-4" style="color: white;">Login</h1>
                                
                                <!-- Error Message if exists -->
                                <#if message?has_content>
                                    <div class="alert alert-danger">
                                        <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                                    </div>
                                </#if>

                                <!-- Login Form -->
                                <form id="kc-form-login" class="login-form" action="${url.loginAction}" method="post">
                                    <!-- E-mail Input -->
                                    <div class="mb-3">
                                        <input tabindex="1" id="username" class="form-control" name="username" value="${(login.username!'')}" autofocus placeholder="E-mail" style="border-radius: 10px; padding: 15px;" />
                                    </div>

                                    <!-- Senha Input with Eye Icon -->
                                    <div class="mb-3 position-relative">
                                        <input tabindex="2" id="password" class="form-control" name="password" type="password" placeholder="Senha" style="border-radius: 10px; padding: 15px;" />
                                        <!-- Icon Eye (for toggling visibility) -->
                                        <span class="position-absolute" style="right: 15px; top: 50%; transform: translateY(-50%);">
                                            <i class="fas fa-eye" style="cursor: pointer;"></i>
                                        </span>
                                    </div>

                                    <!-- Permanecer Conectado Checkbox -->
                                    <div class="form-check mb-4 d-flex justify-content-between gap-2">  
                                        <div>
                                            <input tabindex="3" id="rememberMe" name="rememberMe" type="checkbox" class="form-check-input">
                                            <label for="rememberMe" class="form-check-label" style="color: white;">Permanecer conectado</label>
                                        </div>
                                        <!-- Esqueci minha senha -->
                                        <div class="text-center">
                                            <a href="${url.loginResetCredentialsUrl}" class="forgot-password">Esqueci minha senha</a>
                                        </div>
                                    </div>

                                    <!-- Entrar Button -->
                                    <div class="d-grid mb-3">
                                        <button tabindex="4" class="btn btn-warning btn-block" name="login" id="kc-login" type="submit" style="border-radius: 10px; background-color: #FFC107; color: black; font-weight: bold;">Entrar</button>
                                    </div>
                                </form>

                                <!-- Criar Usuário -->
                                <div class="text-center mt-4">
                                    <h5 class="register-text">ou crie agora mesmo o seu usuário <a class="register-button" href="${url.registrationUrl}">aqui</a></h5>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Footer -->
                <footer class="position-relative pt-4 nav">
                    <div class="w-100">
                        <div class="row align-items-center nav p-3 justify-content-around footer-container">
                            <!-- Footer Logo -->
                                <a href='https://horizontes-empreendedor.site.pencillabs.tec.br/' class="col-12 col-md-3 text-center text-md-left mb-3 mb-md-0">
                                    <img src="${url.resourcesPath}/img/logo.jpeg" alt="Footer Logo" class="img-fluid" style="max-width: 106px;">
                                </a >
                            
                            <!-- Footer Contact -->
                            <div class="col-12 col-md-4 text-center text-md-left mb-3 mb-md-0 footer-contact">
                                <div class="d-flex justify-content-center align-items-center footer-contact__information">
                                    <p class="mb-1">Gama - DF</p>
                                    <p class="mb-1"><a href="mailto:email@empreenda.br" class="text-decoration-none">email@empreenda.br</a></p>
                                </div>
                            </div>
                            
                            <!-- Footer Navigation -->
                            <div class="col-12 col-md-5">
                                <ul class="footer-navigation  d-flex flex-column flex-md-row justify-content-center justify-content-md-center mb-0">
                                    <li class="mx-2"><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/blog/o-que-%C3%A9-o-projeto/" class="text-decoration-none">O QUE É</a></li>
                                    <li class="mx-2"><a href="https://horizontes-empreendedor.moodle.pencillabs.tec.br/" class="text-decoration-none">Área de ensino</a></li>
                                    <li class="mx-2"><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/blog/" class="text-decoration-none">Notícias</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row nav text-center w-100 justify-content-center py-4 rights-reserved__container">
                        <div class="col-12 col-md-6 justify-content-center">
                            <div class="gap-3 justify-content-center d-flex">
                                <p class="mb-0 rights-reserved">©2024 - Plataforma Empreenda</p>
                                <p class="mb-0 rights-reserved">Todos os direitos reservados</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <p class="mb-0 "><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/pol%C3%ADtica-de-privacidade-e-confidencialidade/" class="text-decoration-none rights-reserved">Política de privacidade</a></p>
                        </div>
                    </div>
                </footer>


                
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>
