<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=false displayMessage=false; section>
    <#if section == "title">
        ${msg("loginResetPasswordTitle", (realm.displayName!''))}
    <#elseif section == "form">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <div class="min-vh-100 min-vw-100">
            <div class="background-image position-relative">
                <div class="d-flex justify-content-center align-items-center w-100">

                    <div class="card mt-4 p-4 bg-transparent border-0">
                        <div class="card-body text-center">
                            <!-- Title (Forgot Password) -->
                            <h1 class="mb-4" style="color: white;">Esqueceu sua senha?</h1>

                            <!-- Error Message if exists -->
                            <#if message?has_content>
                                <div class="alert alert-danger">
                                    <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
                                </div>
                            </#if>

                            <!-- Forgot Password Form -->
                            <form id="kc-reset-password-form" class="forgot-password-form" action="${url.loginAction}" method="post">
                                <!-- Username or E-mail Input -->
                                <div class="mb-3">
                                    <input tabindex="1" id="username" class="form-control" name="username" value="${(login.username!'')}" autofocus placeholder="Seu usuário ou e-mail" style="border-radius: 10px; padding: 15px;" />
                                </div>

                                <!-- Submit Button -->
                                <div class="d-grid mb-3">
                                    <button tabindex="2" class="btn btn-warning btn-block" type="submit" style="border-radius: 10px; background-color: #FFC107; color: black; font-weight: bold;">Enviar</button>
                                </div>
                            </form>

                            <!-- Back to Login Link -->
                            <div class="text-center mt-4">
                                <h5><a href="${url.loginUrl}" class="text-decoration-none" style="color: white;">« Voltar ao login</a></h5>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Footer -->
            <footer class="position-relative pt-4 nav">
                <div class="w-100">
                    <div class="row align-items-center nav p-3 justify-content-around footer-container">
                        <!-- Footer Logo -->
                            <a href='https://horizontes-empreendedor.site.pencillabs.tec.br/' class="col-12 col-md-3 text-center text-md-left mb-3 mb-md-0">
                                <img src="${url.resourcesPath}/img/logo.jpeg" alt="Footer Logo" class="img-fluid" style="max-width: 106px;">
                            </a >

                        <!-- Footer Contact -->
                        <div class="col-12 col-md-4 text-center text-md-left mb-3 mb-md-0 footer-contact">
                            <div class="d-flex justify-content-center align-items-center footer-contact__information">
                                <p class="mb-1">Gama - DF</p>
                                <p class="mb-1"><a href="mailto:email@empreenda.br" class="text-decoration-none">email@empreenda.br</a></p>
                            </div>
                        </div>

                        <!-- Footer Navigation -->
                        <div class="col-12 col-md-5">
                            <ul class="footer-navigation d-flex flex-column flex-md-row justify-content-center mb-0">
                                <li class="mx-2"><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/blog/o-que-%C3%A9-o-projeto/" class="text-decoration-none">O QUE É</a></li>
                                <li class="mx-2"><a href="https://horizontes-empreendedor.moodle.pencillabs.tec.br/" class="text-decoration-none">Área de ensino</a></li>
                                <li class="mx-2"><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/blog/" class="text-decoration-none">Notícias</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row nav text-center w-100 justify-content-center py-4 rights-reserved__container">
                    <div class="col-12 col-md-6 justify-content-center">
                        <div class="gap-3 justify-content-center d-flex">
                            <p class="mb-0 rights-reserved">©2024 - Plataforma Empreenda</p>
                            <p class="mb-0 rights-reserved">Todos os direitos reservados</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <p class="mb-0"><a href="https://horizontes-empreendedor.site.pencillabs.tec.br/pol%C3%ADtica-de-privacidade-e-confidencialidade/" class="text-decoration-none rights-reserved">Política de privacidade</a></p>
                    </div>
                </div>
            </footer>

        </div>
    </#if>
</@layout.registrationLayout>
